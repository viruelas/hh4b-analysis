# Toggles for object types
do_small_R_jets: false
do_large_R_Topo_jets: false
do_large_R_UFO_jets: false
do_muons: false
do_electrons: false
do_photons: false
do_taus: false
do_met: false
do_mmc: false
do_overlap_removal: false

# Other global event configs
bypass: false
do_event_cleaning: true
# apply loose jet cleaning only
loose_jet_cleaning: false
do_bbtt_analysis: false
do_bbyy_analysis: false

# Truth-related properties
Truth:
  decayModes: []
  include: DSID_samples.yaml

# Toggles for reconstructed objects properties
Small_R_jet:
  min_pT: 20.e+3
  max_eta: 100
  # nominal btagging working point, used for b-jet pT calibration in particular
  btag_wp: ""
  # additional btagging working points, available for selections and per-jet SFs
  btag_extra_wps: []
  runBJetPtCalib: true
  useFJvt: False
  doL1Matching: False
  doHLTMatching: False
  saveTriggerInfo: False
  # select jet type
  jet_type: 'reco4PFlowJet'
  # calib tool options
  calibToolCalibArea: ""
  calibToolConfigFile: ""
  calibToolCalibSeq: ""
  # uncertainties tool options
  uncertToolCalibArea: ""
  uncertToolConfigPath: ""
  uncertToolMCType: ""
  do_parent_decoration: true
  amount: 0
  amount_bjet: 0
  amount_leadingjet: 0
  systModelJES: "Category"
  systModelJER: "Full"
  btag_egReductionB: "Loose"
  btag_egReductionC: "Loose"
  btag_egReductionLight: "Loose"
  variables_bjets: ["pt", "eta", "phi", "E"]
  variables_allJets: ["pt", "eta", "phi", "E"]
  variables_int_bjets: []
  variables_int_allJets: []

Large_R_jet:
  GN2X_hbb_wps: []
  min_pT: 200.e+3
  max_pT: -1
  max_eta: 100
  runMuonJetPtCorr: false
  do_parent_decoration: true
  amount: 0
  amount_leadingjet: 0
  variables_LargeRJets: []
  variables_int_LargeRJets: []
  wtag_type: ""
  wtag_wp: ""

Photon:
  ID: ""
  Iso: ""
  amount: 0
  variables: ["pt", "eta", "phi", "E", "effSF"]
  variables_int: []
  forceFullSimConfig: false
  correlationModelScale: "1NP_v1"
  extra_wps: []

Electron:
  min_pT: 4.5e+3
  max_eta: 2.47
  trackSelection: true
  maxD0Significance: 5.
  maxDeltaZ0SinTheta: 0.5
  chargeIDSelectionRun2: false
  ID: ""
  Iso: ""
  do_parent_decoration: false
  do_track_decoration: false
  do_IFF_decoration: false
  amount: 0
  variables: ["pt", "eta", "phi", "E", "effSF"]
  variables_int: []
  forceFullSimConfig: false
  correlationModelScale: "1NP_v1"
  correlationModelId: "SIMPLIFIED"
  correlationModelIso: "SIMPLIFIED"
  correlationModelReco: "SIMPLIFIED"
  extra_wps: []

Muon:
  min_pT: 3.e+3
  max_eta: 2.5
  trackSelection: true
  maxD0Significance: 3
  maxDeltaZ0SinTheta: 0.5
  ID: ""
  Iso: ""
  do_parent_decoration: false
  do_IFF_decoration: false
  amount: 0
  variables: ["pt", "eta", "phi", "E", "effSF"]
  variables_int: []
  extra_wps: []

Tau:
  ID: ""
  addMuonRM: false
  do_parent_decoration: false
  amount: 0
  variables: ["pt", "eta", "phi", "E", "effSF"]
  variables_int: ["charge", "nProng", "decayMode"]
  extra_wps: []

Lepton:
  amount: 0
  variables: ["pt", "eta", "phi", "E", "effSF"]
  variables_int: ["charge", "pdgid"]

OverlapRemoval:
  # Enable specific TauAntiTauJet overlap removal, mostly for bbtt analysis
  doTauAntiTauJet: false
  # Enable overlap removal between large-R jets and objects for which 
  # large-R jet OR removal is implemented in Athena
  do_large_R_jets: true
  # Enable overlap removal between small-R jets and large-R jets
  # Athena default is true
  do_small_R_jet_large_R_jet: true

# list of triggers to apply
# "Auto" configures mc from the r-tag, data from year, e.g. data22
trigger_year: Auto
# apply trigger lists to filter event
do_trigger_filtering: false
# list of triggers per year to consider
Trigger:
  selection:
    chains:
      "2015": []
      "2016": []
      "2017": []
      "2018": []
      "2022": []
      "2023": []
      "2029": []

  scale_factor:
    doSF: False

GRL:
  # list of directories to search for files
  years:
    include: grl_years.yaml
  # good runs lists to filter events
  files:
    include: general_grl.yaml
  store_decoration: false

PileupReweighting:
  - postfix: ""
    # In general we should not need custom Pileup Reweighting
    # Uses grl_years to find files
    # prw_files: {}
    # We may need special LumiCalc files depending on triggers
    # Uses grl_years to find files
    # lumicalc_files: {}

# disable CP Algs for calibration
disable_calib: false
# Whether to activate CP alg systematic variations
do_CP_systematics: false
systematics_regex: []
systematics_suffix_separator: "_"
# CutBookKeeper histPattern
cbkHistPattern: "CutBookkeeper_%DSID%_%RUN%_%SYS%"

# Store high level variables by default 
# when the config file set the high level variables
store_high_level_variables: false

# Definition of container names
full_container_names:
  phys:
    include: container-names-DAOD_PHYS.yaml
  physlite:
    include: container-names-DAOD_PHYSLITE.yaml

channels: []
splitOutputTree: false
splitCBK: false

histograms: false
h5:
  n_jets: 10

ttree_output: {}
